﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Exit {

    public string displayName;
    public Room destinationRoom;
    [TextArea(3, 10)]
    public string description;
    public string actionText;
}
