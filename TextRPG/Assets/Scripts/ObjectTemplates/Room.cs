﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextRPG/Room")]
public class Room : ScriptableObject
{
    public string roomName;
    [TextArea(5, 25)]
    public string description;

    public List<Exit> exits;
}



