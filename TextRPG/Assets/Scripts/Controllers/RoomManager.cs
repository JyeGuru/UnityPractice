﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour {

    public Room currentRoom;

    GameController controller;

    void Awake()
    {
        controller = GetComponent<GameController>();
    }

    public void MoveTo(Room room)
    {
        currentRoom = room;
        controller.hasMoved = true;
    }

    public List<Room> FindRoomsByName(string name)
    {
        Room[] allRooms = FindObjectsOfType<Room>();
        List<Room> matchList = new List<Room>();

        for (var i = 0; i < allRooms.Length; i++)
        {
            if (allRooms[i].name == name)
            {
                matchList.Add(allRooms[i]);
            }
        }

        return matchList;
    }

    public void UnpackExits()
    {
        controller.interactionNames.Clear();
        controller.interactionDescriptions.Clear();

        currentRoom.exits.ForEach((exit) =>
        {
            controller.interactionNames.Add(exit.displayName);
            controller.interactionDescriptions.Add(exit.description);
        });
    }
}
