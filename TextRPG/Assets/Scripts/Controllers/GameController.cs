﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public Text logTextUI;
    public List<Button> buttons = new List<Button>();

    public static GameController instance;

    [HideInInspector] public RoomManager roomManager;
    [HideInInspector] public List<string> interactionNames = new List<string>();
    [HideInInspector] public List<string> interactionDescriptions = new List<string>();
    [HideInInspector] public bool hasMoved = false;


    List<string> actionLog = new List<string>();

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

roomManager = GetComponent<RoomManager>();
    }

    void Start()
    {
        RefreshRoom();
        UpdateButtons();
        DisplayLog();
    }

    public void RefreshRoom()
    {
        UnpackRoom();

        string combinedText
            = roomManager.currentRoom.description + "\n"
            + "\n<i>" + string.Join("\n", interactionDescriptions.ToArray()) + "</i>";


        LogStringWithNewline(combinedText);
    }

    void UnpackRoom()
    {
        roomManager.UnpackExits();
    }

    public void DisplayLog()
    {
        logTextUI.text = string.Join("\n", actionLog.ToArray());
    }

    public void LogStringWithNewline(string stringToAdd)
    {
        actionLog.Add(stringToAdd + "\n");
    }

    void UpdateButtons()
    {
        buttons.ForEach((button) =>
        {
            int index = buttons.IndexOf(button);
            if (index < interactionDescriptions.Count)
            {
                string text = interactionNames[index];
                button.GetComponentInChildren<Text>().text = text;
                button.gameObject.SetActive(true);
            }
            else
            {
                button.gameObject.SetActive(false);
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        if (hasMoved)
        {
            RefreshRoom();
            UpdateButtons();
            DisplayLog();
            hasMoved = false;
        }
    }
}
