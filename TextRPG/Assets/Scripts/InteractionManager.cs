﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionManager : MonoBehaviour {

    public void InteractWith(Button button)
    {
        GameController controller = GameController.instance;
        Room currentRoom = controller.roomManager.currentRoom;

        string text = button.GetComponentInChildren<Text>().text;
        Exit exit = currentRoom.exits.Find(e => e.displayName == text);
        if (exit == null)
            Debug.Log("Exit not found!");
        else
        {
            Room here = exit.destinationRoom;
            controller.roomManager.MoveTo(here);
        }
    }

}
