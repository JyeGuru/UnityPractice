﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    public bool isExit = false;
    public int foodValue = 0;
    public AudioClip[] audioClips;

    public void Interact()
    {

        if (this.foodValue > 0)
        {
            GameManager.instance.playerObject.AddFood(foodValue);
            if (audioClips.Length > 0)
            {
                SoundManager.instance.RandomizeSfx(audioClips);
            }
            this.gameObject.SetActive(false);

        }

        if (this.isExit)
        {
            GameManager.instance.Restart();
            GameManager.instance.playerObject.enabled = false;
        }

    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
