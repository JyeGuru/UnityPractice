﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(Room))]
public class ExitEditor : Editor
{
    private ReorderableList list;

    private void OnEnable()
    {

        int expandedLines = 5;
        SerializedProperty prop = serializedObject.FindProperty("exits");
        List<float> heights = new List<float>(prop.arraySize);

        list = new ReorderableList(serializedObject,
                prop,
                true, true, true, true);

        list.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "Exits");
        };

        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);

            float height = EditorGUIUtility.singleLineHeight * expandedLines + 2;

            try
            {
                heights[index] = height;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Debug.LogWarning(e.Message);
            }
            finally
            {
                float[] floats = heights.ToArray();
                Array.Resize(ref floats, prop.arraySize);
                heights = floats.ToList();
            }

            rect.y += 5;

                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, 95, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("displayName"), GUIContent.none);
                EditorGUI.PropertyField(
                    new Rect(rect.x + 100, rect.y, rect.width - 100, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("destinationRoom"), GUIContent.none);
                rect.y += 5;
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight * (expandedLines - 1)),
                    element.FindPropertyRelative("description"), GUIContent.none);
        };


        list.elementHeightCallback = (index) =>
        {
            Repaint();
            float height = 0;

            try
            {
                height = heights[index];
            }
            catch (ArgumentOutOfRangeException e)
            {
                string emsg = e.Message;
                // Commented out, because it fires on every load
                // Debug.LogWarning(emsg);

            }
            finally
            {
                float[] floats = heights.ToArray();
                Array.Resize(ref floats, prop.arraySize);
                heights = floats.ToList();
            }

            return height;
        };

        list.onSelectCallback = (ReorderableList l) => {
            var room = l.serializedProperty.GetArrayElementAtIndex(l.index).FindPropertyRelative("destinationRoom").objectReferenceValue as ScriptableObject;
            if (room)
                EditorGUIUtility.PingObject(room);
        };

        list.onCanRemoveCallback = (ReorderableList l) => {
            return l.count > 1;
        };

        list.onRemoveCallback = (ReorderableList l) =>
        {
            if (EditorUtility.DisplayDialog("Warning!",
                "Are you sure you want to delete this exit?",
                "Yes", "No"))
            {
                ReorderableList.defaultBehaviours.DoRemoveButton(l);
            }
        };

        list.onAddCallback = (ReorderableList l) =>
        {
            var index = l.serializedProperty.arraySize;
            l.serializedProperty.arraySize++;
            l.index = index;
            var element = l.serializedProperty.GetArrayElementAtIndex(index);
            element.FindPropertyRelative("displayName").stringValue = "Exit";
            element.FindPropertyRelative("destinationRoom").objectReferenceValue = null;
            element.FindPropertyRelative("description").stringValue = "PLACEHOLDER EXIT";
        };
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }
}